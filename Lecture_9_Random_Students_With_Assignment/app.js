var students_emails = "luka.shamanauri@gau.edu.ge, valeri.aladashvili@gau.edu.ge, viktoria.shelia@gau.edu.ge, levani.kvirikashvili@gau.edu.ge, konstantine.papaskua@gau.edu.ge, giorgi.asakashvili@gau.edu.ge, demetre.kachkachishvili@gau.edu.ge, sandro.tsverava@gau.edu.ge, saba.gurgenidze@gau.edu.ge, konstantine.khaindrava@gau.edu.ge, giorgi.barnabishvili@gau.edu.ge, giorgi.kevkhishvili@gau.edu.ge, giorgi.abshilava@gau.edu.ge, mariami.chelidze@gau.edu.ge, barbare.jajanidze@gau.edu.ge, sandro.tsereteli@gau.edu.ge"
// console.log(students_emails.split(' '))

function students_array(s_emails){
   // console.log(s_emails)
   let st_emails = s_emails.split(', ')
   // console.log(st_emails)
   let students = []
   for(let i=0; i<st_emails.length; i++){
      // console.log(st_emails[i])
      // console.log(st_emails[i].slice(0, -11).replace(".", " "))
      students.push(st_emails[i].slice(0, -11).replace(".", " "))
   }
   // console.log(students)
   return students
}


// students_array(students_emails);
function get_random_student(students, number){
   // console.log(students)
   let random_students = []
   let random_students_set = new Set()
   // console.log(random_students_set.size)
   // for(let i=0; i<number; i++){
   //    let random = Math.floor(Math.random()*students.length)
   //    random_students.push(students[random])
   //    random_students_set.add(students[random])
   // }
   while (random_students_set.size!=number){
      let random = Math.floor(Math.random()*students.length)
      random_students_set.add(students[random]);
   }
   // console.log("===================")
   // console.log(random_students)
   // console.log(random_students_set)
   // console.log(random_students_set[2])
   // random_students = Array.from(random_students_set).join("; ")
   // console.log(random_students)
   return  Array.from(random_students_set)
}

// get_random_student(students_array(students_emails), 5)

function default_data(){
   var students_fullname = students_array(students_emails)
   // console.log(students_fullname.length)
   var student_numbers = document.getElementById("student_numbers")
   for(let i=1; i<=students_fullname.length; i++) {
      var options = document.createElement("option")
      options.text = i
      student_numbers.add(options)
   }
   // console.log(student_numbers)
}

var assignments = [
                     ["დავალება 1", 17],
                     ["დავალება 2", 19],
                     ["დავალება 3", 12],
                     ["ქვიზი 1", 3],
                     ["შუალედური", 5]
                  ]


function generate(){
   document.querySelector("#result").innerHTML = "";
   var student_numbers = parseInt(document.getElementById("student_numbers").value)
   // console.log(student_numbers)
   // console.log(students_emails)
   var students_fullname = students_array(students_emails)
   // console.log(students_fullname)
   var random_students_data = get_random_student(students_fullname, student_numbers)
   // console.log(random_students_data)
   var table =  document.createElement("table");
   table.classList.add("table_of_results");
   // console.log(table);
   for (let i=0; i<random_students_data.length; i++){
      // console.log(random_students_data[i])
      let tr = document.createElement("tr")

      let td_for_student = document.createElement("td")
      td_for_student.innerText = random_students_data[i]
      tr.appendChild(td_for_student)

      let index_of_a = generate_random_numbers(0, assignments.length-1)
      // console.log(index_of_a);
      let random_assignment = assignments[index_of_a]
      // console.log(random_assignment)
      let td_for_assignment = document.createElement("td")
      td_for_assignment.innerText = random_assignment[0]
      let td_for_assignment_number = document.createElement("td")
      td_for_assignment_number.innerText = generate_random_numbers(1, random_assignment[1])
      tr.appendChild(td_for_assignment)
      tr.appendChild(td_for_assignment_number)
      table.appendChild(tr)
   }
   // console.log(table)
   document.querySelector("#result").appendChild(table)
}

function generate_random_numbers(n, m){
      let random = Math.floor(Math.random()*(m-n+1)+parseInt(n))
      return random
}

